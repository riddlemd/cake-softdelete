<?php
namespace Riddlemd\SoftDelete\Model\Behavior;

use Cake\ORM\Behavior as BaseBehavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\I18n\Time;
use Cake\ORM\Table;

class SoftDeleteBehavior extends BaseBehavior
{
    protected $_defaultConfig = [
        'columns' => [
            'deletedOn' => 'deleted_on',
            'deletedBy' => 'deleted_by_user_id'
        ]
    ];

    public function beforeFind(Event $event, Query $query, \ArrayObject $options, $primary)
    {
        if(isset($options['showDeleted']) && $options['showDeleted'])
            return;
        
        $table = $this->_table;
        $schema = $table->getSchema();
        $deletedOnField = $this->getConfig('columns.deletedOn');

        if(!$schema->getColumn($deletedOnField))
            return;

        $query->andWhere([
            "{$table->alias()}.{$deletedOnField} IS null"
        ]);
    }

    public function beforeDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if(isset($options['forceDelete']) && $options['forceDelete'])
            return;
        
        $table = $this->_table;
        $schema = $table->getSchema();
        $deletedOnField = $this->getConfig('columns.deletedOn');

        if(!$schema->getColumn($deletedOnField))
            return;
        
        $deletedByField = $this->getConfig('columns.deletedBy');
        $entity->$deletedOnField = Time::now();

        if(!empty($options['user']->id))
            $entity->$deletedByField = $options['user']->id;

        if(!$table->save($entity))
            return false;
        
        $event->stopPropagation();
        return true;
    }
}